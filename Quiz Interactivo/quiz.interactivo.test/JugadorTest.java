package quiz.interactivo.test;

import static org.junit.Assert.*;

import org.junit.Test;

import quiz.interactivo.mundo.Jugador;
import quiz.interactivo.mundo.Puntuacion;

public class JugadorTest {

	private Jugador jugador;
	private Puntuacion puntuacion;
	private Puntuacion puntuacion2;
	
	
	@Test
	public void testGetUsuario() {
		jugador= new Jugador( "usuario1", "contraseña1");
		assertEquals("El usuario deberia ser usuario1","usuario1",jugador.getUsuario());
		
	}
	@Test
	public void testGetContraseña() {
		jugador= new Jugador( "usuario1", "contraseña1");
		assertEquals("El usuario deberia ser usuario1","contraseña1",jugador.getContraseña());
	}
	
	
}
