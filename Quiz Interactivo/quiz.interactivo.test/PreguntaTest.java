package quiz.interactivo.test;

import static org.junit.Assert.*;

import org.junit.Test;


import junit.framework.TestCase;
import quiz.interactivo.mundo.Pregunta;
import quiz.interactivo.mundo.Examen;
import quiz.interactivo.mundo.Respuesta;

public class PreguntaTest {

	
	private Examen examen;

	private void setupEscenario1( ) throws Exception
	{
		examen = new Examen( );
	}



	@Test
	public void testInicializacion( )
	{
		try
		{
			setupEscenario1( );
			assertEquals( "El número de preguntas debe ser 5", 10, examen.darCantidadPreguntas( ) );
			assertNotNull( "La pregunta 1 no debe ser null", examen.darPregunta( 0 ) );
			assertNotNull( "La pregunta 2 no debe ser null", examen.darPregunta( 1 ) );
			assertNotNull( "La pregunta 3 no debe ser null", examen.darPregunta( 2 ) );
			assertNotNull( "La pregunta 4 no debe ser null", examen.darPregunta( 3 ) );
			assertNotNull( "La pregunta 5 no debe ser null", examen.darPregunta( 4 ) );
		}
		catch( Exception e )
		{
			fail( "Error al cargar el examen" );
		}
	}

	@Test
	public void testRespuesta( )
	{
		try
		{
			setupEscenario1( );
			Pregunta pregunta = examen.darPregunta( 0 );
			Respuesta respuesta = ( Respuesta )pregunta.darRespuestas( ).get( 0 );
			assertEquals( "La letra de la respuesta es inválida", "a", respuesta.darLetraRespuesta( ) );
			assertEquals( "El texto de la respuesta es inválido", "Tejido", respuesta.darTexto( ) );
		}
		catch( Exception e )
		{
			fail( "Error al cargar el examen" );
		}
	}

}
