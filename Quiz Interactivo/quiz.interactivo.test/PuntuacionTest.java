package quiz.interactivo.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


import quiz.interactivo.mundo.Puntuacion;

public class PuntuacionTest {
	private Puntuacion puntuacion;
	
	@Test
	public void testGetPuntuacion() {
		puntuacion= new Puntuacion("usuario", 5);
		assertEquals("El numero de aciertos deberia ser 5",5, puntuacion.getPuntuacion());
	}
}

