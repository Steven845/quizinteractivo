package quiz.interactivo.interfaz;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import quiz.interactivo.connection.ConnectionBD;

import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import javax.swing.ImageIcon;

public class InterfazLogin extends JFrame {
	
	ConnectionBD cc = new ConnectionBD();
	Connection con=cc.getCon();
	
	

	private JPanel contentPane;
	private JTextField txtUsuario;
	private JPasswordField psContraseña;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazLogin loging = new InterfazLogin();
					loging.setLocationRelativeTo(null);
					loging.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InterfazLogin() {
		
		setTitle("Login");
		
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 486, 276);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 153, 0));
		panel.setBounds(0, 0, 470, 237);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel labUsiario = new JLabel("Usuario:");
		labUsiario.setFont(new Font("Malgun Gothic", Font.BOLD, 16));
		labUsiario.setBounds(96, 35, 73, 25);
		panel.add(labUsiario);
		
		JLabel labContraseña = new JLabel("Contrase\u00F1a: ");
		labContraseña.setFont(new Font("Malgun Gothic", Font.BOLD, 16));
		labContraseña.setBounds(71, 71, 101, 25);
		panel.add(labContraseña);
		
		txtUsuario = new JTextField();
		txtUsuario.setBounds(179, 40, 180, 20);
		panel.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		JButton btnAcceder = new JButton("Acceder");
		btnAcceder.setIcon(new ImageIcon(InterfazLogin.class.getResource("/Imagenes/acceder.png")));
		btnAcceder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if(darUsuario().equals("") || darContraseña().equals("")) {	
						JOptionPane.showMessageDialog(null, "Llene los campos solicitados");
					} else {
						validarUsuario();			
					}
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
		
			}
		});
		btnAcceder.setBackground(new Color(255, 255, 255));
		btnAcceder.setFont(new Font("Malgun Gothic", Font.BOLD, 15));
		btnAcceder.setBounds(71, 119, 126, 29);
		panel.add(btnAcceder);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setIcon(new ImageIcon(InterfazLogin.class.getResource("/Imagenes/cancelar1.png")));
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnCancelar.setBackground(new Color(255, 255, 255));
		btnCancelar.setFont(new Font("Malgun Gothic", Font.BOLD, 16));
		btnCancelar.setBounds(257, 119, 136, 29);
		panel.add(btnCancelar);
		
		psContraseña = new JPasswordField();
		psContraseña.setBounds(179, 76, 180, 20);
		panel.add(psContraseña);
		
		JButton btnRegJugador = new JButton("Registrar");
		btnRegJugador.setIcon(new ImageIcon(InterfazLogin.class.getResource("/Imagenes/adelante.png")));
		btnRegJugador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					InterfazRegJugador iRegJugador = new InterfazRegJugador();
					iRegJugador.setLocationRelativeTo(null);
					iRegJugador.setVisible(true);
					dispose();
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnRegJugador.setBackground(new Color(255, 255, 255));
		btnRegJugador.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		btnRegJugador.setBounds(158, 192, 136, 34);
		panel.add(btnRegJugador);
		
		JLabel labRegJugador = new JLabel("\u00BFAun no tienes cuenta? da clic en el boton Registrar");
		labRegJugador.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		labRegJugador.setBounds(71, 169, 362, 25);
		panel.add(labRegJugador);
		
		JLabel labDatos = new JLabel("Introduzca sus datos");
		labDatos.setFont(new Font("Malgun Gothic", Font.BOLD, 12));
		labDatos.setBounds(40, 11, 136, 14);
		panel.add(labDatos);
	}
	
	
	public String darUsuario() {
		String usuario = txtUsuario.getText();
		return usuario;
	}
	
	public String darContraseña() {
		char[] contraseña = psContraseña.getPassword();
		String pass = new String(contraseña);
		return pass;
	}
	
	public void validarUsuario() {
		
		String usuario=txtUsuario.getText();
		String pass= String.valueOf(psContraseña.getPassword());
		
		try {
		
		String SQL="SELECT  *FROM db_usuarios.tbl_login where usuario='"+usuario+"'AND password='"+pass+"' ";
		Connection conexion=ConnectionBD.getCon();
		PreparedStatement st=conexion.prepareStatement(SQL);
		ResultSet rs=st.executeQuery();
		
		if(rs.next()) {
			 usuario = rs.getString(1);
			 pass = rs.getString(2);
			 
				 InterfazQuizInteractivo iQuizInteractivo = new InterfazQuizInteractivo();
				 iQuizInteractivo.setVisible(true);
				 iQuizInteractivo.setLocationRelativeTo(null);
				 this.dispose();
			 
		 }else {
			 JOptionPane.showMessageDialog(null, "Error de acceso, usuario no registrado");
		 }
			
		}catch(Exception e) {
			JOptionPane.showMessageDialog(null, "Error "+ e.getMessage());
		}
	}
}
