

package quiz.interactivo.interfaz;

import java.awt.*;
import java.util.ArrayList;

import javax.swing.*;

import quiz.interactivo.mundo.Pregunta;
import quiz.interactivo.mundo.Respuesta;

/**
 * Panel que muestra una pregunta y sus posibles respuestas
 */
public class PanelPregunta  extends JPanel
{
	//-----------------------------------------------------------------
	// Atributos
	//-----------------------------------------------------------------
	
	/**
	 * Ventana principal de la aplicaci�n
	 */
	private InterfazExamen ventana;
	private InterfazExamen2 ventana2;
	private InterfazExamen2 ventana3;
	private InterfazExamen2 ventana4;
	
    //-----------------------------------------------------------------
    // Atributos de interfaz
    //-----------------------------------------------------------------
	
	/**
	 * Etiqueta del n�mero de la pregunta
	 */
	private JLabel etiquetaNumeroPregunta;
	
	/**
	 * Etiqueta del texto de la pregunta
	 */
	private JLabel etiquetaTextoPregunta;
	
	/**
	 * Etiqueta con las posibles respuestas
	 */
	private JTextArea textoPosiblesRespuestas;
	
    //-----------------------------------------------------------------
    // Constructor
    //-----------------------------------------------------------------
	
	/**
	 * Construye la pregunta y las posibles respuestas
	 */
	public PanelPregunta( Pregunta pregunta )
	{
		
		// Muestra el n�mero de la pregunta
		this.etiquetaNumeroPregunta = new JLabel("  "+Integer.toString(pregunta.darNumeroPregunta()));
		
		// Muestra el texto de la pregunta
		this.etiquetaTextoPregunta = new JLabel("  "+pregunta.darTexto());
		
		// Muestra las posibles respuestas
		String respuestas="";
		for( int i=0; i<pregunta.darRespuestas().size(); i++ )
		{
			Respuesta respuesta = pregunta.darRespuestas().get(i);
			respuestas+= respuesta.darLetraRespuesta()+". "+respuesta.darTexto()+'\n';
		}
		
		this.textoPosiblesRespuestas = new JTextArea(respuestas);
		this.textoPosiblesRespuestas.setEditable(false);
		this.textoPosiblesRespuestas.setFont(new Font("Arial", Font.BOLD, 17));
		
		// Agrega los elementos
		
		setLayout(new BorderLayout());
		
		JPanel panelTextoPregunta = new JPanel();
		
		panelTextoPregunta.setLayout(new FlowLayout( FlowLayout.LEADING ));	
		
		panelTextoPregunta.setBackground( Color.ORANGE );
		this.etiquetaNumeroPregunta.setFont( new Font("Arial", Font.BOLD, 28) );
		this.etiquetaNumeroPregunta.setForeground(Color.WHITE);
		
		this.etiquetaTextoPregunta.setForeground(Color.WHITE);
		this.etiquetaTextoPregunta.setFont(new Font("Arial", Font.BOLD, 17));
		
		panelTextoPregunta.add(this.etiquetaNumeroPregunta);
		panelTextoPregunta.add(this.etiquetaTextoPregunta);
		
		add(panelTextoPregunta, BorderLayout.NORTH);
		
		JPanel panelPosiblesRespuestas = new JPanel();
		panelPosiblesRespuestas.setBackground( Color.WHITE );
		
		panelPosiblesRespuestas.setLayout(new FlowLayout(FlowLayout.LEADING, 100, 10));
		panelPosiblesRespuestas.add(this.textoPosiblesRespuestas);
		add(panelPosiblesRespuestas, BorderLayout.CENTER);
		
	}
	
	//-----------------------------------------------------------------
	// M�todos
	//-----------------------------------------------------------------
	
	/**
	 * Muestra la pregunta correspondiente en el panel de preguntas
	 * <b>Pre: </b> El panel pregunta ha sido inicializado.
	 * <b>Post: </b> La pregunta correspondiente est� siendo mostrada en el panel de preguntas
	 * @param pregunta El objeto Pregunta que va a ser mostrado al usuario. pregunta != null.
	 */
	public void repintar(Pregunta pregunta)
	{
		
		// Muestra el n�mero de la pregunta
		this.etiquetaNumeroPregunta.setText("  "+Integer.toString(pregunta.darNumeroPregunta()));
		
		// Muestra el texto de la pregunta
		this.etiquetaTextoPregunta.setText("  "+pregunta.darTexto());
		
		// Muestra las posibles respuestas
		String respuestas="";
		for( int i=0; i<pregunta.darRespuestas().size(); i++ )
		{
			Respuesta respuesta = pregunta.darRespuestas().get(i);
			respuestas+= respuesta.darLetraRespuesta()+". "+respuesta.darTexto()+'\n';
		}
		this.textoPosiblesRespuestas.setText(respuestas);
		
	}

}
