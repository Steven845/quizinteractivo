package quiz.interactivo.interfaz;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import quiz.interactivo.connection.ConnectionBD;
import quiz.interactivo.mundo.Puntuacion;

import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class InterfazResultados extends JFrame {

	private InterfazExamen iE1 = new InterfazExamen();
	private InterfazExamen2 iE2 = new InterfazExamen2();
	private InterfazExamen3 iE3 = new InterfazExamen3();
	private InterfazExamen4 iE4 = new InterfazExamen4();
	
	private JPanel contentPane;
	private JTextField txtUsuario;
	private JTextField txtPuntuacion;

	
	
	
	/**
	 * Create the frame.
	 */
	public InterfazResultados() {
		setTitle("Resultados");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 619, 257);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 153, 0));
		panel.setBounds(0, 0, 603, 215);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel labNombreJugador = new JLabel("Nombre del Jugador:");
		labNombreJugador.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		labNombreJugador.setBounds(107, 62, 141, 19);
		panel.add(labNombreJugador);
		
		txtUsuario = new JTextField();
		txtUsuario.setBackground(new Color(255, 255, 255));
		txtUsuario.setEditable(false);
		txtUsuario.setBounds(258, 63, 245, 20);
		panel.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		JLabel labPuntuacion = new JLabel("Puntuaci\u00F3n");
		labPuntuacion.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		labPuntuacion.setBounds(107, 104, 141, 19);
		panel.add(labPuntuacion);
		
		txtPuntuacion = new JTextField();
		txtPuntuacion.setBackground(new Color(255, 255, 255));
		txtPuntuacion.setEditable(false);
		txtPuntuacion.setColumns(10);
		txtPuntuacion.setBounds(258, 105, 245, 20);
		panel.add(txtPuntuacion);
		
		mostrarDatos();
		JButton btnSalir = new JButton("Salir");
		btnSalir.setIcon(new ImageIcon(InterfazResultados.class.getResource("/Imagenes/salir.png")));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
			
		
		});
		btnSalir.setBackground(new Color(255, 255, 255));
		btnSalir.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		btnSalir.setBounds(39, 159, 107, 40);
		panel.add(btnSalir);
		
		JButton btnListaMejPuntuaciones = new JButton("Ver Mejores Puntuaciones");
		btnListaMejPuntuaciones.setIcon(new ImageIcon(InterfazResultados.class.getResource("/Imagenes/ver.png")));
		btnListaMejPuntuaciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					InterfazListaMPuntuaciones iLMPuntuaciones = new InterfazListaMPuntuaciones();
					iLMPuntuaciones.setLocationRelativeTo(null);
					iLMPuntuaciones.setVisible(true);
					dispose();
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnListaMejPuntuaciones.setBackground(new Color(255, 255, 255));
		btnListaMejPuntuaciones.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		btnListaMejPuntuaciones.setBounds(191, 160, 233, 40);
		panel.add(btnListaMejPuntuaciones);
		
		JButton btnJugar = new JButton("Jugar ");
		btnJugar.setIcon(new ImageIcon(InterfazResultados.class.getResource("/Imagenes/jugar.png")));
		btnJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					InterfazQuizInteractivo iQuizIntervo = new InterfazQuizInteractivo();
					iQuizIntervo.setLocationRelativeTo(null);
					iQuizIntervo.setVisible(true);
					dispose();
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnJugar.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		btnJugar.setBackground(Color.WHITE);
		btnJugar.setBounds(453, 159, 118, 40);
		panel.add(btnJugar);
	}

	public void mostrarDatos() {
		
		String usu = iE1.usu;
		int pun = iE1.pun;
		
		if(iE1.usu != ""){
			usu = iE1.usu;
			pun = iE1.pun;
		}else if(iE2.usu != "") {
			usu = iE2.usu;
			pun = iE2.pun;
		}else if(iE3.usu != "") {
			usu = iE3.usu;
			pun = iE3.pun;
		}else if(iE4.usu != "") {
			usu = iE4.usu;
			pun = iE4.pun;
		}
		
		
		try {
		    String SQL="SELECT  *FROM db_usuarios.tbl_puntuaciones where usuario='"+ usu + "'AND puntuacion='"+pun+"' ";
			Connection conexion=ConnectionBD.getCon();
			PreparedStatement st=conexion.prepareStatement(SQL);
			ResultSet rs=st.executeQuery();
			if(rs.next()) {
				usu = rs.getString(2);
				pun = rs.getInt(3);
				 
				this.txtUsuario.setText(usu);
				this.txtPuntuacion.setText(String.valueOf(pun));
			}
			}catch(Exception e) {
				JOptionPane.showMessageDialog(null, "Error "+ e.getMessage());
			}
	
	}

}
