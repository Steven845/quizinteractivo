package quiz.interactivo.interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.border.EmptyBorder;

import quiz.interactivo.connection.ConnectionBD;

import javax.swing.*;

import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class InterfazRegJugador extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsuario;
	private JPasswordField psContraseña;
	
	ConnectionBD cc = new ConnectionBD();
	Connection con=cc.getCon();

	/**
	 * Create the frame.
	 */
	public InterfazRegJugador() {
		
		setTitle("Registro Jugador");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 243);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 153, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel labUsuario = new JLabel("Usuario:");
		labUsuario.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		labUsuario.setBounds(65, 64, 70, 24);
		contentPane.add(labUsuario);
		
		JLabel labRegJugador = new JLabel("Bienvenido a esta aventura, introduce tus datos");
		labRegJugador.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		labRegJugador.setBounds(79, 29, 312, 24);
		contentPane.add(labRegJugador);
		
		txtUsuario = new JTextField();
		txtUsuario.setBounds(145, 68, 246, 20);
		contentPane.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		JLabel labContraseña = new JLabel("Contrase\u00F1a:");
		labContraseña.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		labContraseña.setBounds(44, 100, 91, 24);
		contentPane.add(labContraseña);
		
		JButton btnRegJugador = new JButton("Registrar");
		btnRegJugador.setIcon(new ImageIcon(InterfazRegJugador.class.getResource("/Imagenes/agregar.png")));
		btnRegJugador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				agregarJugador();
			}
		});
		btnRegJugador.setBackground(new Color(255, 255, 255));
		btnRegJugador.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		btnRegJugador.setBounds(65, 160, 126, 33);
		contentPane.add(btnRegJugador);
		
		JButton btnRegresar = new JButton("Regresar");
		btnRegresar.setIcon(new ImageIcon(InterfazRegJugador.class.getResource("/Imagenes/regresar.png")));
		btnRegresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					InterfazLogin login = new InterfazLogin();
					login.setLocationRelativeTo(null);
					login.setVisible(true);
					dispose();
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnRegresar.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		btnRegresar.setBackground(new Color(255, 255, 255));
		btnRegresar.setBounds(271, 160, 130, 33);
		contentPane.add(btnRegresar);
		
		psContraseña = new JPasswordField();
		psContraseña.setBounds(145, 104, 246, 20);
		contentPane.add(psContraseña);
	}
	
	
	public String darUsuario() {
		String usuario = txtUsuario.getText();
		return usuario;
	}
	
	public String darContraseña() {
		char[] contraseña = psContraseña.getPassword();
		String pass = new String(contraseña);
		return pass;
	}
	//nuevo
	public void agregarJugador() {
		String usuario=txtUsuario.getText();
		
		if(comprobarUsuarioRegistrado(txtUsuario.getText())==0) {
			
			
			String SQL="insert into tbl_login (usuario, password) values(?,?)";
		
		
		try {
			
			
			if(darUsuario().equals("") || darContraseña().equals("") ){
				JOptionPane.showMessageDialog(null, "Debe ingresar los datos.",
						"Agregar Jugador",JOptionPane.ERROR_MESSAGE);
			}else {
			PreparedStatement pst=con.prepareStatement(SQL);
	
	
			pst.setString(1, txtUsuario.getText());
			pst.setString(2, String.valueOf(psContraseña.getPassword()));
			pst.executeUpdate();
			JOptionPane.showMessageDialog(null, "Registro exitoso");
			InterfazLogin login = new InterfazLogin();
			login.setLocationRelativeTo(null);
			login.setVisible(true);
			dispose();
			}
		}catch(Exception e) {
			JOptionPane.showMessageDialog(null, "Error de registro "+ e.getMessage());
		}
		}else {
			JOptionPane.showMessageDialog(null, "Usuario ya registrado",
					"Agregar Jugador",JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public int comprobarUsuarioRegistrado(String usuario) {
		Connection conexion=ConnectionBD.getCon();
		PreparedStatement st=null;
		ResultSet rs=null;
		String sql="SELECT count(idtbl_login) FROM db_usuarios.tbl_login WHERE usuario=?";
		try {
			st=conexion.prepareStatement(sql);
			st.setString(1, usuario);
			rs=st.executeQuery();
			if(rs.next()) {
				return rs.getInt(1);
			}
			return 1;
		}catch(SQLException ex) {
			return 1;
		}	
	}

}
