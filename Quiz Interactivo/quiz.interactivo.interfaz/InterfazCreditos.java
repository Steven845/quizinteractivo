package quiz.interactivo.interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import javax.swing.JTextPane;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InterfazCreditos extends JFrame {

	private JPanel contentPane;


	/**
	 * Create the frame.
	 */
	public InterfazCreditos() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 689, 509);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel labImagen = new JLabel("");
		labImagen.setIcon(new ImageIcon(InterfazCreditos.class.getResource("/Imagenes/logoespe.png")));
		labImagen.setBounds(82, 11, 509, 138);
		contentPane.add(labImagen);
		
		JLabel lblNewLabel = new JLabel("Quiz Interactivo \" ORION\"");
		lblNewLabel.setFont(new Font("Malgun Gothic", Font.BOLD, 14));
		lblNewLabel.setBounds(49, 160, 200, 46);
		contentPane.add(lblNewLabel);
		
		JTextPane txtpnProgramaDesarrolladoPor = new JTextPane();
		txtpnProgramaDesarrolladoPor.setFont(new Font("Malgun Gothic", Font.BOLD, 12));
		txtpnProgramaDesarrolladoPor.setText("Programa desarrollado por estudiantes de segundo nivel de la carrera de Ingenieria en Tecnologias de la Informaci\u00F3n y Comunicacion (ITIN) de la Universidad de  las Fuerzas Armadas ESPE sede Santo Domingo.\r\n\r\nDesarrolladores:\r\n     - Jordan Espinosa\r\n     - Genesis Heredia\r\n     - Melany Lopez\r\n     - Luis Paredez\r\n     - Steven Barragan");
		txtpnProgramaDesarrolladoPor.setBounds(49, 202, 614, 194);
		contentPane.add(txtpnProgramaDesarrolladoPor);
		
		JButton btnNewButton = new JButton("Regresar");
		btnNewButton.setIcon(new ImageIcon(InterfazCreditos.class.getResource("/Imagenes/regresar.png")));
		btnNewButton.setBackground(new Color(255, 153, 0));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					InterfazQuizInteractivo iQuizInteractivo = new InterfazQuizInteractivo();
					iQuizInteractivo.setLocationRelativeTo(null);
					iQuizInteractivo.setVisible(true);
					dispose();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
			}
		});
		btnNewButton.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		btnNewButton.setBounds(506, 426, 131, 33);
		contentPane.add(btnNewButton);
	}
}
