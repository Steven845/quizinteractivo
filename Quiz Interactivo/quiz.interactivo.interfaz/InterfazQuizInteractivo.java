package quiz.interactivo.interfaz;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Label;


import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;
import java.awt.event.ActionEvent;

public class InterfazQuizInteractivo extends JFrame {

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public InterfazQuizInteractivo() {
		
		setTitle("Quiz Interactivo");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 740, 539);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(192, 192, 192));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 724, 96);
		panel.setForeground(Color.BLACK);
		panel.setToolTipText("");
		panel.setBackground(new Color(255, 165, 0));
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Bienvenido a Ori\u00F3n");
		lblNewLabel.setForeground(new Color(0, 0, 0));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(256, 11, 238, 36);
		lblNewLabel.setFont(new Font("Malgun Gothic", Font.BOLD, 26));
		panel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("\u00BFAceptas el reto?");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Malgun Gothic", Font.BOLD, 22));
		lblNewLabel_1.setBounds(278, 55, 186, 30);
		panel.add(lblNewLabel_1);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(255, 255, 255));
		panel_1.setForeground(new Color(0, 0, 0));
		panel_1.setBounds(0, 95, 724, 416);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JButton btnJugar = new JButton("GO!");
		btnJugar.setIcon(null);
		btnJugar.setForeground(new Color(255, 51, 255));
		btnJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					SeleccionInterfaz frame = new SeleccionInterfaz();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
					dispose();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnJugar.setToolTipText("");
		btnJugar.setBackground(Color.WHITE);
		btnJugar.setFont(new Font("Brush Script MT", Font.BOLD, 47));
		btnJugar.setBounds(307, 142, 122, 85);
		panel_1.add(btnJugar);
		
		JButton BtnCreditos = new JButton("Creditos");
		BtnCreditos.setIcon(new ImageIcon(InterfazQuizInteractivo.class.getResource("/Imagenes/creditos.png")));
		BtnCreditos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					InterfazCreditos iCreditos = new InterfazCreditos();
					iCreditos.setLocationRelativeTo(null);
					iCreditos.setVisible(true);
					dispose();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
			}
		});
		BtnCreditos.setFont(new Font("Malgun Gothic", Font.BOLD, 17));
		BtnCreditos.setBackground(new Color(255, 255, 255));
		BtnCreditos.setBounds(37, 349, 137, 42);
		panel_1.add(BtnCreditos);
		
		JButton btnAyuda = new JButton("Ayuda");
		btnAyuda.setIcon(new ImageIcon(InterfazQuizInteractivo.class.getResource("/Imagenes/ayuda.png")));
		btnAyuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String url =  "src\\Ficheros\\Instructivo.pdf";
					ProcessBuilder p = new ProcessBuilder();
					p.command("cmd.exe", "/c", url);
					p.start();
				}
				catch(Exception e1) {
					e1.printStackTrace();
				}
				
			}
		});
		btnAyuda.setFont(new Font("Malgun Gothic", Font.BOLD, 17));
		btnAyuda.setBackground(new Color(255, 255, 255));
		btnAyuda.setBounds(539, 349, 137, 42);
		panel_1.add(btnAyuda);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.setIcon(new ImageIcon(InterfazQuizInteractivo.class.getResource("/Imagenes/salir.png")));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnSalir.setFont(new Font("Malgun Gothic", Font.BOLD, 17));
		btnSalir.setBackground(Color.WHITE);
		btnSalir.setBounds(307, 349, 122, 42);
		panel_1.add(btnSalir);
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setBackground(Color.MAGENTA);
		lblNewLabel_2.setIcon(new ImageIcon(InterfazQuizInteractivo.class.getResource("/Imagenes/529beaa033557dc6930bb36f367cb7b3.jpg")));
		lblNewLabel_2.setBounds(0, -5, 728, 410);
		panel_1.add(lblNewLabel_2);
	}

}
