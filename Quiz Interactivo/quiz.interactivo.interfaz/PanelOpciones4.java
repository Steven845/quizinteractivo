
package quiz.interactivo.interfaz;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

import javax.swing.*;
import javax.swing.border.TitledBorder;

import quiz.interactivo.mundo.Pregunta;

/**
 * Panel que permite al usuario introducir una respuesta, pasar a la siguiente o
 * anterior pregunta, terminar el examen y las opciones de extensi�n.
 *
 */
@SuppressWarnings("serial")
public class PanelOpciones4 extends JPanel implements ActionListener
{
	//-----------------------------------------------------------------
	// Constantes
	//-----------------------------------------------------------------
	
	/**
	 * Comando siguiente pregunta
	 */
	private static final String SIGUIENTE = "SIGUIENTE";
	
	/**
	 * Comando anterior pregunta
	 */
	private static final String ANTERIOR = "ANTERIOR";
	
	/**
	 * Comando reiniciar examen
	 */
	private static final String REINICIAR = "REINICIAR";
	
	/**
	 * Comando terminar examen
	 */
	private static final String TERMINAR = "TERMINAR";
	
	
	/**
	 * Comando ir a pregunta
	 */
	private static final String IR_A_PREGUNTA = "IR_A_PREGUNTA";
	
	private static final String CORRECTA = "CORRECTA";
	
	
	//-----------------------------------------------------------------
	// Atributos
	//-----------------------------------------------------------------
	
	/**
	 * Ventana principal de la aplicaci�n
	 */
	private InterfazExamen4 ventana4;

	private Pregunta pregunta;
	
	
	//-----------------------------------------------------------------
	// Atributos de interfaz
	//-----------------------------------------------------------------
	
	/**
	 * Etiqueta Selecci�n
	 */
	private JLabel etiquetaSeleccion;
	
	/**
	 * Campo para la respuesta seleccionada
	 */
	private JTextField campoSeleccion;
	
	/**
	 * Bot�n Siguiente pregunta
	 */
	private JButton btnSiguiente;
	
	/**
	 * Bot�n Anterior pregunta
	 */
	private JButton btnAnterior;
	
	/**
	 * Bot�n de Reiniciar
	 */
	private JButton btnReiniciar;
	
	/**
	 * Bot�n Terminar examen
	 */
	private JButton btnTerminar;
	
	
	/**
	 * Etiqueta con el n�mero de preguntas
	 */
	private JLabel etiquetaConNumeroDePreguntas;
	
	/**
	 * Etiqueta para indicar que ingrese un n�mero de pregunta
	 */
	private JLabel etiquetaNumeroPregunta;
	
	/**
	 * Campo para seleccionar la pregunta
	 */
	private JTextField campoNumeroPregunta;
	
	/**
	 * Bot�n ir a Pregunta
	 */
	private JButton btnIrAPregunta;
	private JButton btnCorrecta;
	
	private JTextField campoRespuestaCorrecta;
	
	//-----------------------------------------------------------------
	// Constructores
	//-----------------------------------------------------------------
	
	/**
	 * Constructor del panel
	 * @param laVentana Ventana principal
	 */
	
	
	public PanelOpciones4( InterfazExamen4 laVentana)
	{
		this.ventana4 = laVentana;
		
		// Etiqueta Selecci�n
		this.etiquetaSeleccion = new JLabel("Selecci�n:");
		
		// Campo para Respuesta Seleccionada
		this.campoSeleccion = new JTextField( );
		
		this.campoRespuestaCorrecta = new JTextField( );
		
		// Bot�n Anterior pregunta
		this.btnAnterior = new JButton();
		ImageIcon icono1 = new ImageIcon("src/Imagenes/flechaRegresar.png" );
		this.btnAnterior.setIcon(icono1);
		add(this.btnAnterior);
		this.btnAnterior.setActionCommand( ANTERIOR );
		this.btnAnterior.addActionListener( this );
		btnAnterior.setBackground(Color.WHITE);
		btnAnterior.setForeground(Color.BLACK);

		// Bot�n Siguiente pregunta
		this.btnSiguiente = new JButton();
		ImageIcon icono2 = new ImageIcon("src/Imagenes/flechaSiguiente.png" );
		this.btnSiguiente.setIcon(icono2);
		add(this.btnSiguiente);
		this.btnSiguiente.setActionCommand( SIGUIENTE );
		this.btnSiguiente.addActionListener( this );
		btnSiguiente.setBackground(Color.WHITE);
		btnSiguiente.setForeground(Color.BLACK);

		// Bot�n Reiniciar Examen
		this.btnReiniciar = new JButton("Reiniciar");
		this.btnReiniciar.setActionCommand( REINICIAR );
		this.btnReiniciar.addActionListener(this);
		btnReiniciar.setBackground(Color.WHITE);
		btnReiniciar.setForeground(Color.BLACK);

		// Bot�n Terminar Examen
		this.btnTerminar = new JButton("Finalizar");
		this.btnTerminar.setActionCommand( TERMINAR );
		this.btnTerminar.addActionListener( this );
		btnTerminar.setBackground(Color.WHITE);
		btnTerminar.setForeground(Color.BLACK);

	
		
		// Etiqueta con el n�mero de preguntas
		
		this.etiquetaConNumeroDePreguntas = new JLabel("Total Preguntas: "+this.ventana4.darCantidadPreguntasExamen());
		// Etiqueta con el texto "N�mero de pregunta:"
		this.etiquetaNumeroPregunta = new JLabel("N�mero de la Pregunta: ");
		
		// Campo de texto que recibe el n�mero de pregunta
		this.campoNumeroPregunta = new JTextField( 2 );
		
		// Bot�n Ir A Pregunta
		this.btnIrAPregunta = new JButton("Ir A Pregunta");
		this.btnIrAPregunta.setActionCommand( IR_A_PREGUNTA );
		this.btnIrAPregunta.addActionListener(this);
		btnIrAPregunta.setBackground(Color.WHITE);
		btnIrAPregunta.setForeground(Color.BLACK);

		//Boton Respuesta Correcta
		this.btnCorrecta = new JButton("R. Correcta");
		this.btnCorrecta.setActionCommand( CORRECTA );
		this.btnCorrecta.addActionListener( this );
		btnCorrecta.setBackground(Color.WHITE);
		btnCorrecta.setForeground(Color.BLACK);
		
		// Agrega los elementos
		
		setBorder( new TitledBorder("Opciones") );
		setLayout( new BorderLayout(4, 4) );
		
		JPanel panelSeleccion = new JPanel();
		panelSeleccion.setLayout( new GridLayout(1, 2) );
		panelSeleccion.add(this.etiquetaSeleccion);
		panelSeleccion.add(this.campoSeleccion);
		panelSeleccion.setPreferredSize( new Dimension( 700, 40 ) );
		add(panelSeleccion, BorderLayout.NORTH);
		
		JPanel panelCambioPregunta = new JPanel();
		panelCambioPregunta.setLayout( new GridLayout(1, 6) );
		panelCambioPregunta.add(this.btnAnterior);
		panelCambioPregunta.add(this.btnSiguiente);
		panelCambioPregunta.add(this.btnReiniciar);
		panelCambioPregunta.add(this.btnTerminar);
		panelCambioPregunta.add(this.btnCorrecta);
		panelCambioPregunta.add(this.campoRespuestaCorrecta);
		panelCambioPregunta.setPreferredSize( new Dimension( 700, 40 ) );
		
		add(panelCambioPregunta, BorderLayout.CENTER);
		
		JPanel panelIrAPregunta = new JPanel();
		panelIrAPregunta.setLayout( new GridLayout(1,4) );
		panelIrAPregunta.add(this.etiquetaConNumeroDePreguntas);
		panelIrAPregunta.add(this.etiquetaNumeroPregunta);
		panelIrAPregunta.add(this.campoNumeroPregunta);
		panelIrAPregunta.add(this.btnIrAPregunta);
		panelIrAPregunta.setPreferredSize( new Dimension( 700, 40 ) );
		add(panelIrAPregunta, BorderLayout.SOUTH);
		
	}
	
	//-----------------------------------------------------------------
	// M�todos
	//-----------------------------------------------------------------
	
	
	/**
	 * Asigna el valor de la respuesta seleccionada
	 * @param laRespuesta Respuesta seleccionada
	 */
	public void establecerRespuestaSeleccionada(String laRespuesta)
	{
		this.campoSeleccion.setText(""+laRespuesta);
	}
	public void repintar(String respuesta) {


		this.campoRespuestaCorrecta.setText(""+respuesta);

	}
	
	public void limpiarRespuestaCorrecta() {
		this.campoRespuestaCorrecta.setText("");
	}
	
	/**
	 * Responde a los eventos de los botones del panel
	 * @param evento Evento generado por un bot�n. evento != null.
	 */
	public void actionPerformed( ActionEvent evento )
	{
		String comando = evento.getActionCommand();
		String respuestaSeleccionada = this.campoSeleccion.getText();
	
		

		if( comando.equals( SIGUIENTE ))
		{
			// Pasa a la siguiente pregunta
			
			this.ventana4.siguientePregunta(respuestaSeleccionada);
			limpiarRespuestaCorrecta();
			
		} 
		else if( comando.equals( ANTERIOR ))
		{
			// Pasa a la pregunta anterior
		
			this.ventana4.anteriorPregunta(respuestaSeleccionada);
			limpiarRespuestaCorrecta();
			
		}
		else if( comando.equals( REINICIAR ))
		{
			// Reinicia el examen y muestra la primera pregunta
		
			this.ventana4.reiniciarExamen(respuestaSeleccionada);
	
		}
		else if( comando.equals( TERMINAR ))
		{
			// Termina el examen y muestra el puntaje al usuario
		
			this.ventana4.terminarExamen(respuestaSeleccionada);
			
		}
		
		else if( comando.equals( IR_A_PREGUNTA ))
		{
			String numeroPreguntaSeleccionado = this.campoNumeroPregunta.getText();			
			
			// M�todo para ir a determinada pregunta
			
			this.ventana4.irAPreguntaExamen(numeroPreguntaSeleccionado);
			
		}
		else if( comando.equals(CORRECTA ))
		{		
			
			this.ventana4.mostrarRespuestaCorrecta();
		
			
		}
	}

}
