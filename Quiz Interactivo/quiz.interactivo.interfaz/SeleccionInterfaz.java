package quiz.interactivo.interfaz;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Dimension;

public class SeleccionInterfaz extends JFrame {

	private JPanel contentPane;
	private JButton btnRegresar;

	/**
	 * Create the frame.
	 */
	public SeleccionInterfaz() {
		setTitle("Temas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 740, 550);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 153, 0));
		panel.setForeground(new Color(255, 153, 0));
		panel.setBounds(61, 52, 263, 160);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JButton btnHistoria = new JButton("New button");
		btnHistoria.setIcon(new ImageIcon(SeleccionInterfaz.class.getResource("/Imagenes/Historia.jpeg")));
		btnHistoria.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					InterfazExamen4 iPHistoria = new InterfazExamen4();
					iPHistoria.setLocationRelativeTo(null);
					iPHistoria.setVisible(true);
					dispose();
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnHistoria.setBounds(10, 11, 243, 138);
		panel.add(btnHistoria);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(255, 153, 0));
		panel_1.setBounds(405, 52, 238, 160);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JButton btnGeografia = new JButton("New button");
		btnGeografia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					InterfazExamen2 iPGeografia = new InterfazExamen2();
					iPGeografia.setLocationRelativeTo(null);
					iPGeografia.setVisible(true);
					dispose();
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnGeografia.setIcon(new ImageIcon(SeleccionInterfaz.class.getResource("/Imagenes/Geografia.jpeg")));
		btnGeografia.setBounds(10, 5, 218, 144);
		panel_1.add(btnGeografia);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(255, 153, 0));
		panel_2.setBounds(61, 250, 263, 177);
		contentPane.add(panel_2);
		panel_2.setLayout(null);
		
		JButton btnMatematicas = new JButton("New button");
		btnMatematicas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					InterfazExamen3 iPMatematicas = new InterfazExamen3();
					iPMatematicas.setLocationRelativeTo(null);
					iPMatematicas.setVisible(true);
					dispose();
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}	
			}
		});
		btnMatematicas.setIcon(new ImageIcon(SeleccionInterfaz.class.getResource("/Imagenes/Matematicas.jpeg")));
		btnMatematicas.setBounds(10, 11, 243, 161);
		panel_2.add(btnMatematicas);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(255, 153, 0));
		panel_3.setBounds(405, 250, 238, 177);
		contentPane.add(panel_3);
		panel_3.setLayout(null);
		
		JButton btnCiencias = new JButton("New button");
		btnCiencias.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					InterfazExamen iPCiencias = new InterfazExamen();
					iPCiencias.setLocationRelativeTo(null);
					iPCiencias.setVisible(true);
					dispose();
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
			}
		});
		btnCiencias.setIcon(new ImageIcon(SeleccionInterfaz.class.getResource("/Imagenes/Ciencias.jpeg")));
		btnCiencias.setBounds(10, 5, 218, 161);
		panel_3.add(btnCiencias);
		
		JLabel lblNewLabel = new JLabel("SELECCIONE UN TEMA");
		lblNewLabel.setSize(new Dimension(0, 2));
		lblNewLabel.setPreferredSize(new Dimension(109, 20));
		lblNewLabel.setMaximumSize(new Dimension(109, 20));
		lblNewLabel.setFont(new Font("Malgun Gothic", Font.BOLD, 16));
		lblNewLabel.setBackground(Color.ORANGE);
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBounds(273, 11, 187, 30);
		contentPane.add(lblNewLabel);
		
		JLabel labHistoria = new JLabel("HISTORIA");
		labHistoria.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		labHistoria.setForeground(Color.WHITE);
		labHistoria.setBounds(158, 223, 134, 14);
		contentPane.add(labHistoria);
		
		JLabel lblNewLabel_2 = new JLabel("GEOGRAFIA");
		lblNewLabel_2.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setBounds(485, 223, 103, 14);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("MATEMATICAS");
		lblNewLabel_3.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		lblNewLabel_3.setForeground(Color.WHITE);
		lblNewLabel_3.setBounds(144, 427, 103, 30);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("CIENCIAS");
		lblNewLabel_4.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		lblNewLabel_4.setForeground(Color.WHITE);
		lblNewLabel_4.setBounds(498, 431, 70, 23);
		contentPane.add(lblNewLabel_4);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(Color.BLACK);
		panel_4.setBounds(214, 468, 313, 33);
		contentPane.add(panel_4);
		panel_4.setLayout(null);
		
		btnRegresar = new JButton("Regresar");
		btnRegresar.setIcon(new ImageIcon(SeleccionInterfaz.class.getResource("/Imagenes/regresar.png")));
		btnRegresar.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		btnRegresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					InterfazQuizInteractivo iQuizInteractivo = new InterfazQuizInteractivo();
					iQuizInteractivo.setLocationRelativeTo(null);
					iQuizInteractivo.setVisible(true);
					dispose();
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnRegresar.setForeground(Color.BLACK);
		btnRegresar.setBackground(new Color(255, 255, 255));
		btnRegresar.setBounds(-21, 0, 131, 34);
		panel_4.add(btnRegresar);
		
		JButton btnSalir = new JButton("SALIR");
		btnSalir.setIcon(new ImageIcon(SeleccionInterfaz.class.getResource("/Imagenes/salir.png")));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnSalir.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		btnSalir.setForeground(Color.BLACK);
		btnSalir.setBackground(new Color(255, 255, 255));
		btnSalir.setBounds(167, 0, 131, 34);
		panel_4.add(btnSalir);
	}

}
