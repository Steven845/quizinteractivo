package quiz.interactivo.interfaz;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import quiz.interactivo.connection.ConnectionBD;
import quiz.interactivo.mundo.Puntuacion;

import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.border.BevelBorder;
import javax.swing.ImageIcon;

public class InterfazListaMPuntuaciones extends JFrame {

	private Puntuacion puntuacion = null;
	private JPanel contentPane;
	private JScrollPane barra;
	private JTable tblPuntuaciones;
	/**
	 * Create the frame.
	 */
	public InterfazListaMPuntuaciones() {
		setTitle("Mejores Puntuaciones");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 633, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 255, 255));
		panel.setBounds(0, 0, 617, 336);
		contentPane.add(panel);
		panel.setLayout(null);
		
		barra = new JScrollPane();
		barra.setBounds(10, 11, 597, 261);
		getContentPane().add(barra);
		
		
		
		JButton btnVerPuntuaciones = new JButton("Ver Puntuaciones");
		btnVerPuntuaciones.setIcon(new ImageIcon(InterfazListaMPuntuaciones.class.getResource("/Imagenes/ver.png")));
		btnVerPuntuaciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				construirTabla();
			}
		});
		btnVerPuntuaciones.setBackground(new Color(255, 153, 0));
		btnVerPuntuaciones.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		btnVerPuntuaciones.setBounds(387, 284, 194, 41);
		panel.add(btnVerPuntuaciones);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.setIcon(new ImageIcon(InterfazListaMPuntuaciones.class.getResource("/Imagenes/salir.png")));
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnSalir.setBackground(new Color(255, 153, 0));
		btnSalir.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		btnSalir.setBounds(53, 285, 102, 40);
		panel.add(btnSalir);
		
		JButton btnRegresar = new JButton("Regresar");
		btnRegresar.setIcon(new ImageIcon(InterfazListaMPuntuaciones.class.getResource("/Imagenes/regresar.png")));
		btnRegresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					InterfazResultados iResultados = new InterfazResultados();
					iResultados.setLocationRelativeTo(null);
					iResultados.setVisible(true);
					dispose();
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnRegresar.setFont(new Font("Malgun Gothic", Font.BOLD, 13));
		btnRegresar.setBackground(new Color(255, 153, 0));
		btnRegresar.setBounds(209, 285, 123, 40);
		panel.add(btnRegresar);
	}

	
	public void construirTabla() {
		String titulos[] = {"Usuario", "Puntuación"};
		String informacion[][] = mostrarPuntuaciones();
		
		tblPuntuaciones = new JTable(informacion, titulos);
		barra.setViewportView(tblPuntuaciones);
		
	}
	
	public String[][] mostrarPuntuaciones() {
	
		ArrayList<Puntuacion> puntuaciones = new ArrayList<Puntuacion>();
		
		try {
		    String SQL="SELECT  * FROM tbl_puntuaciones ORDER BY puntuacion DESC";
			Connection conexion=ConnectionBD.getCon();
			PreparedStatement st=conexion.prepareStatement(SQL);
			ResultSet rs=st.executeQuery();
			while(rs.next()) {
				puntuacion = new Puntuacion();
				puntuacion.setUsuario(rs.getString("usuario"));
				puntuacion.setPuntuacion(Integer.parseInt(rs.getString("puntuacion")));
				puntuaciones.add(puntuacion);
			}
			rs.close();
			st.close();
			}catch(Exception e) {
				JOptionPane.showMessageDialog(null, "Error "+ e.getMessage());
			}
		
		
		String matrizInfo[][] = new String[puntuaciones.size()][2];
		for(int i = 0; i < puntuaciones.size(); i++) {
			matrizInfo[i][0] = puntuaciones.get(i).getUsuario() + "";
			matrizInfo[i][1] = puntuaciones.get(i).getPuntuacion() + "";
		}
		
		return matrizInfo;
	}
	
}
