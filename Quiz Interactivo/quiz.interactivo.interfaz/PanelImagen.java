
package quiz.interactivo.interfaz;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

/**
 * Panel de la imagen del t�tulo
 *
 */
@SuppressWarnings("serial")
public class PanelImagen extends JPanel
{
	//-----------------------------------------------------------------
	// Atributos de la Interfaz
	//-----------------------------------------------------------------
	
	/**
	 * Imagen del t�tulo
	 */
	private JLabel imagen;
	
	//-----------------------------------------------------------------
	// Constructores
	//-----------------------------------------------------------------
	
	/**
	 * Constructor sin par�metros
	 */
	public PanelImagen( )
	{
		FlowLayout layout = new FlowLayout( );
        layout.setHgap( 0 );
        layout.setVgap( 0 );
        setLayout( layout );
        setPreferredSize( new Dimension( 800, 170) );

        //Carga la imagen
        ImageIcon icono = new ImageIcon( "data/icono2.png" );

        //La agrega a la etiqueta
        imagen = new JLabel( "" );
        imagen.setIcon( icono );
        add( imagen );

        //Color de fondo blanco
        setBackground( Color.ORANGE );
        setBorder( new LineBorder( Color.WHITE ) );
        
        
	}

}
