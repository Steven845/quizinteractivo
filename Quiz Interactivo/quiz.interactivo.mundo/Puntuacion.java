package quiz.interactivo.mundo;

import java.io.Serializable;

public class Puntuacion extends Jugador {

	private int puntuacion;
	
	
	public Puntuacion() {
		super();
		this.puntuacion = 0;
	}

	public Puntuacion(String usuario, int puntuacion) {
		super(usuario);
		this.puntuacion = puntuacion;
	}

	public Puntuacion(String usuario, String contraseña, int puntuacion) {
		super(usuario, contraseña);
		this.puntuacion = puntuacion;
		
	}

	public int getPuntuacion() {
		return puntuacion;
	}

	public void setPuntuacion(int puntuacion) {
		this.puntuacion = puntuacion;
	}

	@Override
	public String toString() {
		return "Puntuacion [puntuacion=" + puntuacion + "]";
	}

}
