package quiz.interactivo.mundo;

import java.io.Serializable;

public class Jugador {
	
	protected String usuario;
	protected String contraseña;
	
	
	public Jugador() {
		usuario = "";
		contraseña = "";
	}

	public Jugador(String usuario) {
		this.usuario = usuario;
		this.contraseña = "";
	}


	public Jugador(String usuario, String contraseña) {
		super();
		this.usuario = usuario;
		this.contraseña = contraseña;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	@Override
	public String toString() {
		return "Jugador [Usuario=" + usuario + ", Contraseña=" + contraseña + "]";
	}

}
