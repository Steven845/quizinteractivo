

package quiz.interactivo.mundo;

import java.util.Properties;

/**
 * Representa una respuesta a una pregunta
 */
public class Respuesta
{

    //-----------------------------------------------------------------
    // Atributos
    //-----------------------------------------------------------------

    private String texto;
    private String letraRespuesta;

    //-----------------------------------------------------------------
    // Constructores
    //-----------------------------------------------------------------

    /**
     * Crea una nueva respuesta leyendo los valores de un archivo
     */
    public Respuesta( int elNumeroPregunta, int elNumeroRespuesta, Properties persistencia )
    {
        letraRespuesta = darLetra( elNumeroRespuesta );
        texto = persistencia.getProperty( "examen.pregunta" + elNumeroPregunta + ".respuesta_" + letraRespuesta + ".texto" );
    }

    //-----------------------------------------------------------------
    // M�todos
    //-----------------------------------------------------------------

    /**
     * Devuelve la letra de la respuesta en la pregunta
     */
    public String darLetraRespuesta( )
    {
        return letraRespuesta;
    }

    /**
     * Devuelve el texto de la respuesta
     */
    public String darTexto( )
    {
        return texto;
    }

    /**
     * Devuelve la letra representada por el numero especificado. 
     */
    private String darLetra( int numero )
    {
        return Character.toString( ( char ) ( 'a' + numero ) );
    }
    
}
