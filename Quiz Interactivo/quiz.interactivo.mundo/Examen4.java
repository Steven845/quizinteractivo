

package quiz.interactivo.mundo;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Properties;

/**
 * Representa el examen completo
 */
public class Examen4
{
    //-----------------------------------------------------------------
    // Atributos
    //-----------------------------------------------------------------

    /**
     * Preguntas del examen. Contiene objetos de tipo Pregunta.
     */
    private ArrayList<Pregunta> preguntas;

    //-----------------------------------------------------------------
    // Constructores
    //-----------------------------------------------------------------

    /**
     * Crea un nuevo examen cargando la información del archivo
     */
    public Examen4( ) throws Exception
    {
        preguntas = new ArrayList<Pregunta>( );
        
        //Carga la información
        Properties persistencia = new Properties( );
        FileInputStream fis = new FileInputStream( new File( "data/examen4.data" ) );
        persistencia.load( fis );
        fis.close( );
     
        //
        //Lee la información
        int cantidadPreguntas = Integer.parseInt( persistencia.getProperty( "examen.cantidadPreguntas" ) );
        for( int i = 1; i <= cantidadPreguntas; i++ )
        {
            Pregunta pregunta = new Pregunta( i, persistencia );
            preguntas.add( pregunta );
        }
    }
    
    

    //-----------------------------------------------------------------
    // Métodos
    //-----------------------------------------------------------------

    /**
     * Devuelve la cantidad de preguntas del examen
     */
    public int darCantidadPreguntas( )
    {
        return preguntas.size( );
    }

    /**
     * Devuelve la pregunta en el numero especificado.
     */
    public Pregunta darPregunta( int numero )
    {
        return ( Pregunta )preguntas.get( numero );
    }
    
    /**
     * Guarda la pregunta dada en la lista de preguntas del examen
     */
    public void guardarPregunta( int numero, Pregunta pregunta )
    {
    	this.preguntas.set(numero, pregunta);
    }

    /**
     * Devuelve el puntaje del examen sobre 100 
     */
    public int darPuntaje( )
    {
        int cuenta = 0;
        for( int i = 0; i < preguntas.size(); i++ )
        {
            Pregunta pregunta = ( Pregunta )preguntas.get(i);
            if( pregunta.respuestaCorrecta( ) )
            {
                cuenta++;
            }
        }
        return ( cuenta * 100 ) / preguntas.size( );
    }
    public String darRespuestaCo(Pregunta pregunta) {
    	String respuestac="";
    	for( int i = 0; i < preguntas.size(); i++ )
        {
           
            respuestac= pregunta.darRespuestaCorrecta();
            
        }
    return respuestac;
    
    }
    /**
     * Reinicia el examen creando una nueva lista de preguntas y
     */
    public void reiniciar() throws Exception
    {
    	// Crea una nueva lista de preguntas    	
    	this.preguntas = new ArrayList<Pregunta>();
    	
    	//Carga la información
        Properties persistencia = new Properties( );
        FileInputStream fis = new FileInputStream( new File( "data/examen4.data" ) );
        persistencia.load( fis );
        fis.close( );
        
        //Lee la información
        int cantidadPreguntas = Integer.parseInt( persistencia.getProperty( "examen.cantidadPreguntas" ) );
        for( int i = 1; i <= cantidadPreguntas; i++ )
        {
            Pregunta pregunta = new Pregunta( i, persistencia );
            preguntas.add( pregunta );
        }
    }
    }